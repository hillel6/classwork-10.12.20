import re

from django.db.models.signals import pre_save
from django.dispatch import receiver

from home.models import Student

# Делаем из функции сигнал добавив декоратор receiver
#    то на какое действие сигналу срабатывать
#            |       то к какой модели добавить сигнал
#            |          |
@receiver(pre_save, sender=Student)
def pre_save_artifact(sender, instance, **kwargs):  # noqa

    # Как пример удаление спец символов и приведение к нижнему регистру
    # то как здесь удаляются спец символы обращать внимение не стоит, будем это проходить чуть позже
    instance.normalized_title = re.sub('[^\w\s]|_', '', instance.title).lower()

# также чтобы сигналы начали работать нужно сделать их импорт в apps.py
# и указать default_app_config в __init__.py приложения
